# home server

I'm starting to remove away from my home server where everything is running on a single machine to a raspberry pi cluster with k3s. I'll desribe here everything as detailed as I can.

My goal is that the following applications will run on my cluster and that you can run this home server as well.

* [x] [Pi-hole](./pihole/README.md)
* [x] [Radicale](./radicale/README.md)
* [x] [Pinry](./pinry/README.md)
* [ ] Velero
* [ ] Seafile
* [ ] OpenHAB
* [ ] gogs
* [ ] drone.io
* [ ] homematic

Fell free to contribute.

## Dependencies

* install *k3s* on a Raspberry Pi 4

    ~~~bash
    curl -sfL https://get.k3s.io | sh -

    # enable cgroup_memory
    sudo sed -i -e 1's/.*/cgroup_enable=memory &/' /boot/cmdline.txt

    # Enabling legacy iptables on Raspbian Buster
    sudo iptables -F
    sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
    sudo update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
    sudo reboot
    ~~~

* copy *k3s.yaml* file locally (Debian/Ubuntu)
    
    ~~~bash
    sudo scp /etc/rancher/k3s/k3s.yaml lookslikematrix@lookslikematrix-desktop:/home/lookslikematrix/.kube/config
    ~~~

* install *kubectl* locally (Debian/Ubuntu)

    ~~~bash
    sudo apt-get update && sudo apt-get install -y apt-transport-https gnupg2
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
    echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
    sudo apt-get update
    sudo apt-get install -y kubectl
    ~~~

* change IP-Address in *confg*-file
    
    ~~~bash
    sed -i 's/127.0.0.1/<SERVER_IP_ADDRESS>/g' ~/.kube/config
    ~~~

* install *helm* locally (Debian/Ubuntu)

    ~~~bash
    curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
    sudo apt-get install apt-transport-https --yes
    echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
    sudo apt-get update
    sudo apt-get install helm
    ~~~

## How to create certificates

If you like you can protect your home server via HTTPS. Therefore you have to create a root certificate and for every service a extra certificate. Than you have to add your root certificate to your browser and operating system and you will have a secure connection.

On a Debian system you can create your certificates with *openssl*.

* create a root certificate and remember the password because you will need it for every creation of a new certificate.

    ~~~bash
    openssl genrsa -out rootkey.pem 4096
    openssl req -x509 -new -nodes -extensions v3_ca -key rootkey.pem -days 1024 -out rootcertificate.pem -sha512
    ~~~

* install [*cert-manager*](https://cert-manager.io/) via helm

    ~~~bash
    kubectl create namespace cert-manager
    helm repo add jetstack https://charts.jetstack.io
    helm repo update
    helm install cert-manager jetstack/cert-manager --namespace cert-manager --version v1.1.0 -set installCRDs=true
    cp cert-manager/cert-manager-secret.yaml cert-manager/cert-manager-secret.local.yaml
    ~~~

* create a file with the base64 string of the *rootkey.pem* and the *rootcertificate.pam*. You can find this file also under *cert-manager/cert-manager-secret.yaml*

    ~~~yaml
    apiVersion: v1
    kind: Secret
    metadata:
        name: ca-key-pair
        namespace: cert-manager
    data:
        tls.crt: <insert output from `base64 rootcertificate.pem -w0`>
        tls.key: <insert output from `base64 rootkey.pem -w0`>
    ~~~

* add secret to your cluster

    ~~~bash
    kubectl apply -f cert-manager/cert-manager-secret.local.yaml
    ~~~

* create a CA cluster issuer

    ~~~bash
    kubectl apply -f cert-manager/ca-issuer.yaml
    ~~~

* enable TLS in the service value files

## How to force HTTPS

* force *Traefik* to redirect *HTTP* to *HTTPS* inside of *k3s*. Access your cluster node via ssh and create the following file with *sudo nano /var/lib/rancher/k3s/server/manifests/traefik-config.yaml*. I stored the file also in this repository under *k3s/traefik-ingress.yaml* so you can also just copy it.
    
    ~~~yaml
    apiVersion: helm.cattle.io/v1
    kind: HelmChartConfig
    metadata:
        name: traefik
        namespace: kube-system
    spec:
        valuesContent: |-
            ssl:
                enabled: true
                enforced: true
                permanentRedirect: true
    ~~~
