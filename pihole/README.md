# Pi-hole

When you like to install Pi-Hole at the moment you have to do the following steps:

~~~bash
git clone https://gitlab.com/lookslikematrix/home-server.git
cd home-server/pihole
cp values.yaml values.local.yaml
~~~

Make changes to the *values.local.yaml* file in the *pihole*-directory, create the package and install or upgrade it.

~~~bash
helm package .
helm upgrade pihole pihole-0.0.15.tgz --namespace pihole --install --create-namespace -f values.local.yaml
~~~

I will create a helm repository for this soon.