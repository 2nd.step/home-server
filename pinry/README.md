# Pinry

When you like to install Pinry at the moment you have to do the following steps:

~~~bash
git clone https://gitlab.com/lookslikematrix/home-server.git
cd home-server/pinry
cp values.yaml values.local.yaml
~~~

Make changes to the *values.local.yaml* file in the *pinry*-directory, create the package and install or upgrade it.

~~~bash
helm package .
helm upgrade pinry pinry-0.0.7.tgz --namespace pinry --install --create-namespace -f values.local.yaml
~~~

Go to your Pi-hole and add your pinry URL (default: pinry.lookslikematrix.local) to the local DNS (Local DNS -> DNS Records) whit the external IP address of the cluster.

I will create a helm repository for this soon.