# Radicale

When you like to install Radicale at the moment you have to do the following steps:

~~~bash
git clone https://gitlab.com/lookslikematrix/home-server.git
cd home-server/radicale
cp values.yaml values.local.yaml
~~~

Make changes to the *values.local.yaml* file in the *radicale*-directory, create the package and install or upgrade it.

~~~bash
helm package .
helm upgrade radicale radicale-0.0.9.tgz --namespace radicale --install --create-namespace -f values.local.yaml
~~~

Go to your Pi-hole and add your radicale URL (default: radicale.lookslikematrix.local) to the local DNS (Local DNS -> DNS Records) whit the external IP address of the cluster.

I will create a helm repository for this soon.